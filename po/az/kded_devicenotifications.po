# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Kheyyam Godjayev <xxmn77@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-07-31 00:42+0000\n"
"PO-Revision-Date: 2024-06-10 12:07+0400\n"
"Last-Translator: Kheyyam Godjayev <xxmn77@gmail.com>\n"
"Language-Team: Azerbaijani <kde-i18n-doc@kde.org>\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.07.70\n"

#: devicenotifications.cpp:333
#, fuzzy, kde-format
#| msgctxt "@title:notifications"
#| msgid "USB Device Detected"
msgctxt "@title:notifications"
msgid "Display Detected"
msgstr "USB cihaz aşkarlandı"

#: devicenotifications.cpp:334
#, fuzzy, kde-format
#| msgid "A USB device has been plugged in."
msgid "A display has been plugged in."
msgstr "USB cihaz qoşuldu."

#: devicenotifications.cpp:347
#, fuzzy, kde-format
#| msgctxt "@title:notifications"
#| msgid "USB Device Removed"
msgctxt "@title:notifications"
msgid "Display Removed"
msgstr "USB cihaz çıxarıldı"

#: devicenotifications.cpp:348
#, fuzzy, kde-format
#| msgid "A USB device has been unplugged."
msgid "A display has been unplugged."
msgstr "USB cihaz çıxarıldı."

#: devicenotifications.cpp:379
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 qoşuldu"

#: devicenotifications.cpp:379
#, kde-format
msgid "A USB device has been plugged in."
msgstr "USB cihaz qoşuldu."

#: devicenotifications.cpp:382
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "USB cihaz aşkarlandı"

#: devicenotifications.cpp:405
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 çıxarıldı."

#: devicenotifications.cpp:405
#, kde-format
msgid "A USB device has been unplugged."
msgstr "USB cihaz çıxarıldı."

#: devicenotifications.cpp:408
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "USB cihaz çıxarıldı"

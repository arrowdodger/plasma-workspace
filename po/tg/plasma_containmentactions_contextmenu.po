# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-26 00:40+0000\n"
"PO-Revision-Date: 2021-09-05 11:38-0700\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: Tajik Language Support\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.1\n"

#: menu.cpp:102
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "Намоиш додани KRunner"

#: menu.cpp:107
#, kde-format
msgid "Open Terminal"
msgstr ""

#: menu.cpp:111
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "Қулфи экран"

#: menu.cpp:120
#, fuzzy, kde-format
#| msgctxt "plasma_containmentactions_contextmenu"
#| msgid "Lock Screen"
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show Logout Screen"
msgstr "Қулфи экран"

#: menu.cpp:125
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Display Configuration"
msgstr ""

#: menu.cpp:285
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "Танзимоти васлкунаки феҳристи мазмун"

#: menu.cpp:295
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[Амалиётҳои дигар]"

#: menu.cpp:298
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "Амалиётҳои тасвири замина"

#: menu.cpp:302
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[Ҷудокунанда]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Leave…"
#~ msgstr "Баромад..."

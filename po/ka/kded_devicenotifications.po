# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-07-31 00:42+0000\n"
"PO-Revision-Date: 2024-07-31 05:30+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: devicenotifications.cpp:333
#, kde-format
msgctxt "@title:notifications"
msgid "Display Detected"
msgstr "აღმოჩენილია ეკრანი"

#: devicenotifications.cpp:334
#, kde-format
msgid "A display has been plugged in."
msgstr "ეკრანი შეერთებულია."

#: devicenotifications.cpp:347
#, kde-format
msgctxt "@title:notifications"
msgid "Display Removed"
msgstr "ეკრანი გამოერთებულია"

#: devicenotifications.cpp:348
#, kde-format
msgid "A display has been unplugged."
msgstr "ეკრანი გამოერთებულია."

#: devicenotifications.cpp:379
#, kde-format
msgid "%1 has been plugged in."
msgstr "შეერთებულია %1."

#: devicenotifications.cpp:379
#, kde-format
msgid "A USB device has been plugged in."
msgstr "შეერთებულია USB მოწყობილობა."

#: devicenotifications.cpp:382
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "აღმოჩენილია USB მოწყობილობა"

#: devicenotifications.cpp:405
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 გამოერთებულია."

#: devicenotifications.cpp:405
#, kde-format
msgid "A USB device has been unplugged."
msgstr "USB მოწყობილობა გამოერთებულია."

#: devicenotifications.cpp:408
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "USB მოწყობილობა გამოერთებულია"
